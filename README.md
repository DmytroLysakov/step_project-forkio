## При роботі над проектом використовувались такі технології:

* Методолгія найменування класів BEM;
* Принцип Mobile first;
* HTML з використанням семантичних тегів;
* CSS препроцесор SASS SCSS:
    * _Variables_
    * _Mixins_
    * _Functions_
    * _Сепарація та імпортування файлів_
* Adaptive and responsive design;
* NPM модулі:
    * _del_
    * _gulp-autoprefixer_
    * _gulp-clean-css_
    * _gulp-file-include_
    * _gulp-group-css-media-queries_
    * _gulp-if_
    * _gulp-imagemin_
    * _gulp-newer_
    * _gulp-notify_
    * _gulp-plumber_
    * _gulp-rename_
    * _gulp-replace_
    * _gulp-sass_
    * _gulp-version-number_
    * _gulp-webp_
    * _gulp-webp-html-nosvg_
    * _gulp-webpcss_
    * _sass_
    * _webp-converter_
    * _webpack_
    * _webpack-stream_
* Проект був зібраний на основі Gulp;
---

Розробив проект: Лисаков Дмитро;

## Виконані завдання

Зверстати шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана).
Зверстати секцію People Are Talking About Fork.
Зверстати блок Revolutionary Editor.
Зверстати секцію Here is what you get.
Зверстати секцію Fork Subscription Pricing.
Налаштувати збірку проекту за допомогою Gulp.
Розмістити проект в інтернеті за допомогою Gitlab pages.

## Посилання на Gitlab pages 
https://dmytrolysakov.gitlab.io/step_project-forkio/