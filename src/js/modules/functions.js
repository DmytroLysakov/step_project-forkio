
export function isWebp() {
	function testWebP(callback) {
		let webP = new Image();
		webP.onload = webP.onerror = function () {
			callback(webP.height == 2);
		};
		webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
	}

	testWebP(function (support) {
		let className = support === true ? 'webp' : 'no-webp';
		document.documentElement.classList.add(className);
	});
}

export function dropdownOpen() {
	let burgerLogo = document.querySelector('.dropdown');
	burgerLogo.addEventListener('click', () => {
		document.querySelector('.dropdown').classList.toggle('dropdown--active');
		document.querySelector('.nav').classList.toggle('nav--active');
	})
}
export function dropdownClose() {
	document.addEventListener('click', (e) => {
		let click = e.composedPath().includes(burgerLogo);
		if (!click) {
			document.querySelector('.dropdown').classList.remove('dropdown--active');
			document.querySelector('.nav').classList.remove('nav--active');
		}
	})
}

export function navbarScroll() {
	window.addEventListener('scroll', navbarScroll);
	function navbarScroll() {
		let header = document.querySelector('.navbar')
		if (window.scrollY >= 30) {
			header.classList.add('navbar--on-scroll')
		} else if (window.scrollY === 0) {
			header.classList.remove('navbar--on-scroll')
		}
	};
}